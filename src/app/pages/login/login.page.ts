import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ConstantsService } from 'src/app/services/constants/constants.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  typeimput:string = 'password';
  constructor(
    public nav:NavController,
    public constants: ConstantsService
  ) {}


  registerNewUser(){
    this.constants.logs("registro nuevo");
    this.nav.navigateForward('new-user')
    
  }

  viewPassword(view){
    this.typeimput = (view)? 'password': 'text';
  }


}
