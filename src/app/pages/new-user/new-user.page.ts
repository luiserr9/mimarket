import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseServiceService } from '../../services/FirebaseService/firebase-service.service'
import { ConstantsService } from 'src/app/services/constants/constants.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { NavController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';





@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.page.html',
  styleUrls: ['./new-user.page.scss'],
})
export class NewUserPage implements OnInit {
  dataUser:FormGroup;
  imgProfile:any;
  typeimput:any="password";
  typeimput2:any="password"
  validEmail:boolean=true;
  equalspassword:boolean=true;
  

  constructor(public formBuilder: FormBuilder,
    public constants: ConstantsService, 
    public firebaseService:FirebaseServiceService,
    private imagePicker: ImagePicker,
    public nav:NavController,
    public actionSheetController: ActionSheetController,
    public _DomSanitizationService: DomSanitizer,
    private camera: Camera ) {

      this.dataUser = this.formBuilder.group({
        name: new FormControl ({value:'',disabled:false},[Validators.required,Validators.minLength(3)]),
        lastname: new FormControl ({value:'',disabled:false},[Validators.required,Validators.minLength(3)]),
        email:new FormControl ({value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.email]),
        password:new FormControl ({value:'',disabled:false},[Validators.required,Validators.minLength(3)]),
        passwordconfirm: new FormControl ({value:'',disabled:false},[Validators.required,Validators.minLength(3)])
      });

   }

  ngOnInit() {
    
  }

  //validacion de campos
  onInputname(event){
    this.dataUser.value.name = event.target.value
    var RegExPattern = /[a-zA-Z ]$/;
    this.validForm(RegExPattern,"name",this.dataUser.value.name,true);  

  }

  onInputlastmane(event){
    this.dataUser.value.lastname = event.target.value
    var RegExPattern = /[a-zA-Z ]$/;
    this.validForm(RegExPattern,"lastname",this.dataUser.value.lastname,true);  


  }

  onInputemail(event){
    this.dataUser.value.email = event.target.value
    var RegExPattern =/[a-zA-Z0-9@._\-]$/
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (emailRegex.test(this.dataUser.value.email)) {
      this.validEmail = true;
    }else{
      this.validEmail = false;
    }
    this.validForm(RegExPattern,"email",this.dataUser.value.email,false);  

  }

  onInputpasswordConfirm(event){
    this.dataUser.value.passwordconfirm = event.target.value;
    this.equalspassword = (this.dataUser.value.password == this.dataUser.value.passwordconfirm)? true : false;
    console.log("caontaseñas XD", this.dataUser.value.password, this.dataUser.value.passwordconfirm, this.equalspassword);
    
    
  }

  // validacion de campos fin

  backpage(){
    //regreso a la pagina anterior
    this.nav.back();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de perfil',
      cssClass: 'match-item-action-sheet',
      buttons: [{
        text: 'Galeria',
        icon: 'image',
        handler: () => {
          this.constants.logs("imagen de galeria");
          this.profilePicture();
        }
      }, {
        text: 'Tomar una foto',
        icon: 'camera',
        handler: () => {
          this.constants.logs("Tomar una foto con la camara");
          this.takephoto();

        }
      },{
        text: 'Cancelar',
        role: 'cancel',
        cssClass:'botoncancelar',
        handler: () => {
          
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  profilePicture(){
   
    var options = {
      maximumImagesCount: 1,
      width: 500,
      height: 500,
      outputType: 1
    };

    this.imagePicker.getPictures(options).then((results) => {
    
      this.imgProfile = "data:image/*;base64,"+results[0];
      this.constants.logs("base64 de la imegn selecionada", this.imgProfile);
    }, (err) => { 
      this.constants.logs('error imagen', err);
    });
    //this.firebaseService.uploadImage("data:;base64,");
  }

  takephoto(){
    this.constants.logs("plugin de camara");
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.constants.logs("foto de camara", base64Image);
     }, (err) => {
      this.constants.logs("error de foto",err);
     });
  }

  viewPassword(pas,view){
    if (pas == 1) {
      this.typeimput = view;
    }else{
      this.typeimput2 = view;
    }
    
  }

  registerUser(){
    this.constants.logs("registro", this.dataUser.controls);
  }
 
  validForm(exp,contol,datafrom,mayus:boolean,validlength?,event?){
    var RegExPattern = exp;
    let string:string;
    if (mayus) {
      string = String(datafrom).toUpperCase();
    }else{
      string =  String(datafrom);
    }
    if (event == undefined) {
      this.dataUser.controls[contol].setValue(string);
      if (!string.match(RegExPattern)) {
        var value = string.substring(0, string.length-1);
        this.dataUser.controls[contol].setValue(value);
      }
    }else{
      event.target.value = event.target.value.replace(RegExPattern, '');
    }
    if (validlength != undefined) {
      if(string.length >= validlength){
        this.dataUser.controls[contol].setValue(string.substring(0, validlength));
      }
    }
  }
}
