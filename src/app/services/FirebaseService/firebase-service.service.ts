import { Injectable } from '@angular/core';
import {AngularFireStorage } from '@angular/fire/storage';


@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {

  constructor( public firebase:AngularFireStorage ) { }

  uploadImage(imageURI){


    console.log("urlimg", imageURI);
    let storageRef = this.firebase.storage.ref();
    let imageRef = storageRef.child('image.jpg');
    console.log("urlimg", imageURI,imageRef);
    
    imageRef.putString(imageURI, 'data_url').then( (snapshot) => {
      console.log(snapshot);
      
    }, (err) => {
      console.log(err);
    })

    storageRef.child('image.jpg').getDownloadURL().then((url) => {
      console.log(url);
      
    }).catch((error)=> {
      console.log(error);
    });

    /*return new Promise<any>((resolve, reject) => {
      let storageRef = this.firebase.storage.ref();
      let imageRef = storageRef.child('image');
      this.encodeImageUri(imageURI, function(image64){
        imageRef.putString(image64, 'data_url').then(snapshot => {
          resolve(snapshot.downloadURL)
        }, err => {
          reject(err);
        })
      })
    })*/
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/png");
      callback(dataURL);
    };
    img.src = imageUri;
  };

  
}
