import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  dev=true;
  prod=false;
  urldev="";
  urlprod="";

  logs(val1=null,val2=null,val3=null,val4=null,val5=null,val6=null,val7=null,val8=null,val9=null,){
    if (this.dev) {
      console.log(
        (val1==null)?'-': val1,
        (val2==null)?'-': val2,
        (val3==null)?'-': val3,
        (val4==null)?'-': val4,
        (val5==null)?'-': val5,
        (val6==null)?'-': val6,
        (val7==null)?'-': val7,
        (val8==null)?'-': val8,
        (val9==null)?'-': val9)
    }
  }

}
